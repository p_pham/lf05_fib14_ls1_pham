/** Fehlersuche
 * @version 3.0 vom 01.12.2020
 * @author Tenbusch
 */  
import java.util.scanner;

public class Aufgabe3 {
    
    public static main(String args) {
    	umfangsberechnung();
    
    
    public static String umfangsberechnung() {  //die Zeile ist korrekt, Finger weg!
    	
    	//Variablendeklaration
    	Scanner scan = new Scanner();
    	final double PI = 3.141;
    	double durchmesser, umfang;
    	String Überschrift = "Umfangsberechnung eines eckigen Kreises";
        
    	//Eingabe
    	System.out.println(Überschrift);
    	System.out.println("Bitte geben Sie den Durchmesser ein: ");
    	durchmesser = scan.nextdouble();
    	
    	//Verarbeitung
    	umfang = 2* PI * durchmesser;
        
    	//Ausgabe
    	System.println("Der Umfang beträgt " + umfang);
    	
    	//Automatisierte Auswertung  ------ab hier ist alles korrekt, nichts verändern------
    	return "A2: " + ueberschrift + ";" + PI + "; Durchmesser: " + durchmesser + "; Umfang: " + umfang;
    }
    
} 
    
}    

