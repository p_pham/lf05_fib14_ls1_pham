/*  Der folgende Code wird die Ausgaben mit den Namen untereinander ausgeben. 
	Anschließend werden drei Ausgaben verglichen. Dabei werden die Argumente 
	in die Formel (arg1 + 8) < (arg2 * 3) gesetzt. Falls diese Gleichung stimmt,  
	wird true ausgegeben, falls nicht, dann false. */
public class Methoden {
public static void main(String[] args) {
ausgabe(1, "Mana");
ausgabe(2, "Elise");
ausgabe(3, "Johanna");
ausgabe(4, "Felizitas");
ausgabe(5, "Karla");
System.out.println(vergleichen(1, 2));
System.out.println(vergleichen(1, 5));
System.out.println(vergleichen(3, 4));
}
public static void ausgabe(int zahl, String name) {
System.out.println(zahl + ": " + name);
}
public static boolean vergleichen(int arg1, int arg2) {
return (arg1 + 8) < (arg2 * 3);
}
}